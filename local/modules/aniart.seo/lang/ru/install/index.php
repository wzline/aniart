<?
$MESS['ANIART_SEO_MODULE_NAME'] = 'AniArt.SEO';
$MESS['ANIART_SEO_MODULE_DESCRIPTION'] = 'Демонастрационный модуль Aniart.SEO';
$MESS['ANIART_SEO_PARTNER_NAME'] = 'AniArt';
$MESS['ANIART_SEO_PARTNER_URI'] = 'https://aniart.com.ua';

$MESS['ANIART_SEO_NEED_ANIART_MAIN'] = 'Для установки модуля AniArt.SEO необходимо сначала установить модуль Базовый модуль Aniart (aniart.main)';
$MESS['ANIART_SEO_MODULE_HL_NOT_INSTALLED'] = 'Для работы модуля AniArt.SEO (aniart.seo) необходим модуль Highload-блоки (highloadblock). Установите модуль Highload-блоки (highloadblock)';
?> 